package com.mavericklabs.appautoupdatesample;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.mavericklabs.app_auto_update.CheckAppUpdate;

public class MainActivity extends AppCompatActivity {
    private CheckAppUpdate checkAppUpdate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkAppUpdate = new CheckAppUpdate(this);
        checkAppUpdate.check();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (checkAppUpdate != null) {
            checkAppUpdate.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkAppUpdate != null) {
            checkAppUpdate.onResume();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (checkAppUpdate != null) {
            checkAppUpdate.onStop();
        }
    }


}
