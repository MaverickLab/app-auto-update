package com.mavericklabs.app_auto_update;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.google.android.play.core.tasks.Task;

import static android.app.Activity.RESULT_OK;
import static com.google.android.play.core.install.model.AppUpdateType.FLEXIBLE;
import static com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE;

/**
 * Created by Prashant Kashetti on 25/9/19.
 */
public class CheckAppUpdate {
    private final int MY_REQUEST_CODE = 1;
    private final int DEFAULT_UPDATE_TYPE = IMMEDIATE;
    private AppUpdateManager appUpdateManager;
    private int appUpdateType = DEFAULT_UPDATE_TYPE;
    private Activity activity;
    private InstallStateUpdatedListener installStateUpdatedListener = new
            InstallStateUpdatedListener() {
                @Override
                public void onStateUpdate(InstallState state) {
                    if (state.installStatus() == InstallStatus.DOWNLOADED) {
                        if (appUpdateType == FLEXIBLE) {
                            popupSnackbarForCompleteUpdate();
                            onStop();
                        }
                    } else if (state.installStatus() == InstallStatus.INSTALLED) {
                        if (appUpdateManager != null) {
                            onStop();
                        }
                    }
                }
            };


    public CheckAppUpdate(AppCompatActivity baseActivity) {
        activity = baseActivity;
    }

    public CheckAppUpdate(AppCompatActivity baseActivity, int appUpdateType) {
        activity = baseActivity;
        this.appUpdateType = appUpdateType;
    }

    /**
     * 1)Create instance AppUpdateManager and register the listener
     * 2)Check for update availability
     * 3)Request an update using startUpdateFlowForResult() method. from this method as per requirement
     * change the appUpdateType parameter.
     */
    public void check() {
        appUpdateManager = AppUpdateManagerFactory.create(activity);
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        appUpdateInfoTask.addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
            @Override
            public void onSuccess(AppUpdateInfo appUpdateInfo) {
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                        && appUpdateInfo.isUpdateTypeAllowed(appUpdateType)) {
                    try {
                        appUpdateManager.startUpdateFlowForResult(
                                appUpdateInfo,
                                appUpdateType,
                                activity,
                                MY_REQUEST_CODE);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }

                } else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                    appUpdateManager.unregisterListener(installStateUpdatedListener);
                    if (appUpdateType == FLEXIBLE) {
                        popupSnackbarForCompleteUpdate();
                    }
                }
            }
        });
    }

    /**
     * If update is downloaded,Checks that the update is not installed during 'onResume()'.
     * If an in-app update is already running, resume the update. using startUpdateFlowForResult() method.
     */
    public void onResume() {
        appUpdateManager
                .getAppUpdateInfo()
                .addOnSuccessListener(
                        new OnSuccessListener<AppUpdateInfo>() {
                            @Override
                            public void onSuccess(AppUpdateInfo appUpdateInfo) {
                                if (appUpdateInfo.updateAvailability()
                                        == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                                    try {
                                        appUpdateManager.startUpdateFlowForResult(
                                                appUpdateInfo,
                                                appUpdateType,
                                                activity,
                                                MY_REQUEST_CODE);
                                    } catch (IntentSender.SendIntentException e) {
                                        e.printStackTrace();
                                    }
                                } else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                                    if (appUpdateType == FLEXIBLE) {
                                        popupSnackbarForCompleteUpdate();
                                    }
                                }
                            }
                        });
    }

    /**
     * Get a callback for update status
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MY_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                if (appUpdateType == IMMEDIATE) {
                    activity.finishAffinity();
                }
            } else {
                appUpdateManager.registerListener(installStateUpdatedListener);
            }
        }
    }

    /**
     * After downloading the update, call the below method  if appUpdateType is flexible
     * then show the snackbar for install update.
     */
    private void popupSnackbarForCompleteUpdate() {
        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) activity
                .findViewById(android.R.id.content)).getChildAt(0);

        Snackbar snackbar =
                Snackbar.make(
                        viewGroup,
                        activity.getString(R.string.new_app_is_ready),
                        Snackbar.LENGTH_INDEFINITE);

        snackbar.setAction(activity.getString(R.string.install), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appUpdateManager != null) {
                    appUpdateManager.completeUpdate();
                }
            }
        });

        snackbar.setActionTextColor(activity.getResources().getColor(R.color.quantum_googred));
        snackbar.show();
    }

    /**
     * unregister the listener.
     */
    public void onStop() {
        if (appUpdateManager != null)
            appUpdateManager.unregisterListener(installStateUpdatedListener);
    }
}
