# App Auto Update

This library is used to check app auto update. 
    
# Requirements
  - Minimum API level 16

# Install
Download via **Gradle**:
```
implementation 'com.mavericklabs:app-auto-update:1.0.4'
```

# Usage
For check app auto update add the following code :
```
CheckAppUpdate checkAppUpdate = new CheckAppUpdate(activityContext); 
checkAppUpdate.check();
```

Override the following methods:

```
 @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            checkAppUpdate.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
            checkAppUpdate.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
            checkAppUpdate.onStop();
    }

```
  


# License

```
Copyright (C) 2019 mavericklabs.in

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```